from django.db import models

# Create your models here.

class UserType(models.Model):
    description = models.TextField(verbose_name="Descripción")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "tipo_usuario"
        verbose_name_plural = "tipos_usuarios"
        ordering = ["-created"]

    def __str__(self):
        return self.description

class UserStatus(models.Model):
    description = models.TextField(verbose_name="Descripción")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "estado_usuario"
        verbose_name_plural = "estados_usuarios"
        ordering = ["-created"]

    def __str__(self):
        return self.description

class User(models.Model):
    username = models.CharField(max_length=40, verbose_name="Nombre de usuario")
    name = models.CharField(max_length=80, verbose_name="Nombre")
    email = models.EmailField(verbose_name="Correo electrónico")
    password = models.CharField(max_length=80, verbose_name="Contraseña")
    type_id = models.ForeignKey(UserType, on_delete=models.PROTECT, verbose_name="ID tipo")
    status_id = models.ForeignKey(UserStatus, on_delete=models.PROTECT, verbose_name="ID estado")
    #Campos para saber cuando se crea y modifica un registro
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "usuario"
        verbose_name_plural = "usuarios"
        ordering = ["-created"]

    def __str__(self):
        return self.name
