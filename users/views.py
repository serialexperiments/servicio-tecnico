from django.shortcuts import render, HttpResponse
from .models import User, UserType, UserStatus

# Create your views here.
def home(request):
    usersType = UserType.objects.all()
    return render(request, 'users/home.html', {'usersType': usersType})
