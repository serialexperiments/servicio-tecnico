from django.contrib import admin
from .models import User, UserType, UserStatus

# Register your models here.

class UserAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

userModels = [
    User,
    UserType,
    UserStatus
]

admin.site.register(userModels, UserAdmin)
